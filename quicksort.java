class quickSortMiddle
{
	public static void main(String[] args)
	{
		int[] arr={15,9,7,14,13,19,7,11,18};
		int leng=arr.length;
		quickSortMiddle qsm=new quickSortMiddle();
		qsm.quickSortRecursion(arr,0,leng-1);
		qsm.printArray(arr);
		
		}
		int partition(int[] arr,int low,int high)
		{
			int pivot=arr[low];
		
			while(low<=high)
			{ 	
				while(arr[low]<pivot)
				{
					low++;
				}
				while(arr[high]>pivot)
				{
				high--;
				}
				if(low<=high)
				{
					int temp=arr[low];
					arr[low]=arr[high];
					arr[high]=temp;
					low++;
					high--;
				
				}
			}
		    return low;
		}
		void quickSortRecursion(int[] arr,int low,int high)
		{
		int pi=partition(arr,low,high);
		if(low<pi-1)
		{quickSortRecursion(arr,low,pi-1); //left hand side 
        }
		if(pi<high)
		{
		quickSortRecursion(arr,pi,high);  //for right hand side
		}
		}
		void printArray(int[] arr)
		{for(int i:arr)
		{System.out.print(i +" ");
}}}